# OpenVPN Ephemeral GitLab Template

## Overview
This repo provides an example template that supports a GitLab pipeline building and destroying an OpenVPN EC2 server on demand. This template makes use of the [paulmarsicloud/openvpn-ephemeral/aws](https://registry.terraform.io/modules/paulmarsicloud/openvpn-ephemeral/aws/latest) Terraform Module and the pipeline is created using the [apline/terragrunt](https://hub.docker.com/r/alpine/terragrunt) Docker container.

## Pre-requisites
In order to utilize this template repo, you will need an AWS Account with an IAM user that has programmatic access, and OpenVPN Connect on your local machine.

## Environment Variables
In order to use this template, simply clone/fork this repo to your own GitLab project and update the following:
1. The `TF_VAR_public_ip: <REPLACE ME>` variable in the [.gitlab-ci.yml](/.gitlab-ci.yml) file with your local public IP address (e.g. `curl 4.ipaddr.io`)
2. Go to Settings > CI/CD > Variables and add both the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` as _protected_ and _masked_ variables like so:
![aws keys image](/readme-images/aws_keys.png)

## Start/Stop OpenVPN Ephemeral Server
1. When ready to use OpenVPN, go to CI/CD > Pipelines and choose the region in which you want to **Build** your OpenVPN server in: ![pipelines](/readme-images/pipelines.png)
2. Click the "Play" button to start the job.
3. When job has succeeded, click on "Browse" in the Job artifacts section: ![artifacts](/readme-images/artifacts.png)
4. Select `openvpn.ovpn` file and download to your local machine: ![job artifact](/readme-images/openvpn-artifact.png)
5. Open `openvpn.ovpn` on your local machine with your OpenVPN Connect application
6. When ready to destroy, simply disconnect from the OpenVPN Connect profile, go to Pipelines and click the "Play" button of the corresponding **Destroy** region
